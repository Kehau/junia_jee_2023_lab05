package junia.lab05.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import junia.lab05.core.entity.BusinessType;

public interface BusinessTypeDAO extends JpaRepository<BusinessType, Long> {

}
